---
layout: handbook-page-toc
title: "Contributor Success Team"
description: "Contributor Success Team"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Child Pages
{:.no_toc}

##### [User Journey](./user-journey.html)
{:.no_toc}

##### [GitLab Community Teams Structure](./team-structure.html)
{:.no_toc}

##### [Sisense SQL snippets](./sisense-sql-snippets.html)
{:.no_toc}

## Common Links

| **GitLab Team Handle** | [`@gitlab-org/quality/contributor-success`](https://gitlab.com/gitlab-org/quality/contributor-success) |
| **Slack Channel** | [`#g_contributor_success`](https://gitlab.slack.com/archives/C02R0NE6P6C), [`#i_user_engagement_contributor_growth`](https://gitlab.slack.com/archives/C0223D98HHC) (standup with the Community Relations team), [`#mrarr-wins`](https://gitlab.slack.com/archives/C01NTMN6U0P) (to announce new MRARR wins) |
| **Team Boards** | [Backlog board](https://gitlab.com/groups/gitlab-com/-/boards/4295302), [Kanban board](https://gitlab.com/groups/gitlab-com/-/boards/4295341), [Technical board](https://gitlab.com/groups/gitlab-org/-/boards/4296693) |
| **Issue Tracker** | [`gitlab-com/quality/contributor-success`](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues) |

- - -

## Strategy

This team directly follows the strategy outlined in our [open source growth strategy](/handbook/engineering/open-source/growth-strategy.html).

## Mission

The goal of the team is to increase the technical improvements and efficiency of our contribution process to sustain
our ambition of [1000+ contributors with merged MRs per month to GitLab](/company/strategy/#2-build-on-our-open-core-strength).

### FY23 Direction

In FY23 we will be focused on growing the number of unique new monthly
contributors, reducing [Open Community MR Age (OCMA)](#open-community-mr-age-ocma) and increasing [MRARR](#mrarr).
This will increase development velocity without requiring additional engineering resources.

Parallel to this we'll increase [observability of community contributed value](#community-contributed-value-observability)
through improving the business intelligence around it. This will allow us to create some
predictability through [foreshadowing](#community-metrics-foreshadowing). These efforts are cross-functional and
require working together with Marketing and Product Development.

This accounts for 70 - 80% of the workload. The remaining 20 - 30% is ad-hoc
work. The ad-hoc work is eclectic and ranges from supporting customers on
contributions, supporting various open source initiatives and supporting the
[Engineering Productivity team](/handbook/engineering/quality/engineering-productivity/).

#### Unique New Monthly Contributors

1. Minimize reliance on human interaction
1. Reduce volatility through introducing [automations](#community-merge-request-workflow-automations) that drive contributions
   forward automatically
1. Capitalize on untapped potential - MRs that have become stale but have received a seal of approval as useful addition to GitLab.
1. Invest into attracting more new contributors

#### Community Contributed Value Observability

1. Introduce measurement points in various places in the contribution pipeline
1. [Collect objective and subjective feedback from contributors about the process](/handbook/engineering/quality/triage-operations/#code-review-experience-feedback)
1. Create insight into actual community contribution pipeline size
1. Standardize contribution MRARR valuation
1. Categorize contribution and measure value per type

#### Community Metrics Foreshadowing

1. Predict community metric trends
1. Empower teams to react to negative trends before they actualize
1. Define ambitious targets for FY24

#### Open Community MR Age (OCMA)

1. Minimize reliance on human factors that contribute to a large standard deviation
1. Identify & drive tooling improvements to decrease OCMA, and collaborate with the [Engineering Productivity team](/handbook/engineering/quality/engineering-productivity/) through shared tooling for [automated triaging](/handbook/engineering/quality/engineering-productivity/#automated-triage-policies)

## Team Responsibilities

* Improve GitLab's Contribution Efficiency and [merge request coaching](/job-families/expert/merge-request-coach/) process.
* Contribute as a [merge request coach](/job-families/expert/merge-request-coach/) in one or more specialties by providing guidance to community contributors on technical and non-technical aspects.
* Be a point of escalation for community contributors and identify GitLab DRIs to resolve blockers.
* Organize community contributors into [community teams](team-structure.html) and ensure their success.
* Track [contribution](/community/contribute/) delivery of the Community Contributors and Cohorts.
* Nominate impactful community contributors and contributions for recognition.
* Collaborate closely with our [Marketing counterparts](/handbook/marketing/community-relations/) and [Core team](/community/core-team/).
* Engineering representative that can ride along with the [Developer evangelist](/job-families/marketing/developer-evangelist/) and [Code contributor program manager](/job-families/marketing/code-contributor-program-manager/).
* Improve community recognition system and awards in collaboration with the [Community Relations team](/handbook/marketing/community-relations/).
* Participate in GitLab's overall Engineering open source outreach events and processes.

## Team Members

<%= direct_team(manager_role: 'Director, Contributor Success') %>

### File an issue

There are 2 GitLab groups where the contributor success team is working with, so it can be confusing to understand what 
to put where. We should strive to be as transparent as possible and default to solutions and discussions that benefit 
the wider community. This is why, if you are in doubt, *we ask you to default to projects in the
[gitlab-org group](https://gitlab.com/gitlab-org)*.

#### gitlab-org/gitlab

All issues that relate to the Open Source project GitLab and that can serve to enhance the contributor flow and are public by nature should be created here by default. We aim to not have any distinction between contributors or GitLab team-members for which we expect by default that everyone should be able to contribute to. 

* Location: [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab)
* Required label: `Contributor Success`

#### gitlab-com/quality/contributor-success

All issues that relate to the inner working of the company GitLab, including specific internal team workings, onboardings-issues or issues relating to customers that should be separated from the Open Source project GitLab can be placed here.

* Location: [gitlab-com/quality/contributor-success](https://gitlab.com/gitlab-com/quality/contributor-success)
* Required label: `Contributor Success`

## Project Management

* Our [team project](https://gitlab.com/gitlab-com/quality/contributor-success) is our single source of truth for all tasks & backlog.
* Epics that contain cross-functional work across multiple departments can be created at the `gitlab-com` [level](https://gitlab.com/groups/gitlab-com/-/epics?label_name[]=Contributor+Success).

This team has the following immediate work items planned.

* [Contribution Efficiency Improvements](https://gitlab.com/groups/gitlab-com/-/epics/1619)
* [Increasing MRARR through internal partnerships](https://gitlab.com/groups/gitlab-com/-/epics/1225)

## OKRs

Every quarter, the team commits to [Objectives and Key Results (OKRs)](/company/okrs/). The below shows current and previous quarter OKRs, it updates automatically as the quarter progresses.

### Current quarter

<iframe src="https://app.ally.io/public/TQRmKHpOC7NoZb7" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>

### Previous quarter

<iframe src="https://app.ally.io/public/e0izWt13OqmGHuO" class="dashboard-embed" height="1600" width="100%" style="border:none;"> </iframe>

## Performance Indicators

We have the following Performance Indicators

#### [Unique Wider Community Contributors per Month](/handbook/engineering/quality/performance-indicators/#unique-wider-community-contributors-per-month)

- Target: increase to be greater than 200 per month by FY23Q4
- Activities:
  - Partnership with Community Relations and Technical Marketing team.
  - Hold community office hours
  - Hold hackathons
  - Allow running of QA tests from forks
  - Shorten the CI runtime for community contributions (in forks)

#### [Open Community MR Age (OCMA)](/handbook/engineering/quality/performance-indicators/#open-community-mr-age)

- Target: decrease to lower than 30 days by FY23Q4
- Activities:
  - Shorten CI time
  - Improve Community Contribution automation
  - Enable running QA tests on forks
  - Increase number of coaches
  - Partner with Engineering Productivity to provide feedback to improve contribution tooling (currently GDK).      

#### [MRARR](/handbook/engineering/quality/performance-indicators/#mrarr)

- Target: increase to 400M MR$ by FY23Q4
- Activities:
  - Reach out to top tier enterprise customers
  - Help take-on inactive customer contribution to completion & merge
  - Partner with TAMs to enlist and facilitate contribution from customers
  - Launch contribution materials targeting large enterprises
  - Partner with Community relations team (David P)
  - Maintain a list of known contributors with a mapping to their accounts and the accounts ARR contribution as input to this KPI  

#### [Community Coaches per Month](/handbook/engineering/quality/performance-indicators/#community-mr-coaches-per-month)

- Target: increase to be greater than 50 per month by FY23Q3
- Activities:
  - Work with Development Department (Christopher L, VP of Development) for volunteers.
  - Work with UX Department (Christie L, VP of UX) Christie for volunteers.
  - Refresh MR coaches as “Community coaches” so non-code review work can be encouraged (design, etc)
  - Launch training materials for coaches

#### [Community Contribution MRs as Features per Month](/handbook/engineering/quality/performance-indicators/#community-contribution-mrs-added-as-features-per-month)

- Target: increase to 30% by FY23Q2
- Activities:
  - Encourage features at Community relations hackathons.
  - Published list of feature issues with Marketing team.

## Working with community contributions

### Community contribution labels

- The `Community contribution` label is automatically applied by the [GitLab Bot](https://gitlab.com/gitlab-bot) to MRs submitted by wider community members. 
  * You can see the list of MRs in [`gitLab-org` list of merge requests](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name[]=Community+contribution).
  * [Learn more about the cadence and conditions for this automation](/handbook/engineering/quality/triage-operations/#community-contributions).
- The `1st contribution` label is added to first-time contributions. Every time a contributor is opening a merge request under the `gitlab-org` namespace for the first time, the label `1st contribution` is automatically applied to the merge request.
  - You can see the list of MRs in [`gitlab-org` list of merge requests](https://gitlab.com/groups/gitlab-org/-/merge_requests?label_name%5B%5D=1st+contribution).
  - [First-time contributors](/handbook/marketing/community-relations/code-contributor-program/#first-time-contributors) are also awarded a gift as our way to say thanks.

### Community merge request workflow automations

Community merge requests are MRs opened by a person that's not a member of the [`gitlab-org`](https://gitlab.com/-/ide/project/gitlab-org) nor [`gitlab-com`](https://gitlab.com/-/ide/project/gitlab-com) groups.

#### Triage reports

See [Community-related triage reports](/handbook/engineering/quality/triage-operations/#community-related-triage-reports).

#### Scheduled workflow automation

See [Community-related scheduled workflow automation](/handbook/engineering/quality/triage-operations/#community-related-scheduled-workflow-automation).

#### Reactive workflow automation

See [Community-related reactive workflow automation](/handbook/engineering/quality/triage-operations/#community-related-reactive-workflow-automation).

### Merge request coaches

[Merge request coaches](/job-families/expert/merge-request-coach/) are available to help contributors with their MRs. This includes: 
- Identifying reviewers for the MR.
- Answering questions from contributors.
- Educating contributors on the [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
- Or completing the MR if the contributor is unresponsive or unable to complete.
  - In that case, the `coach will finish` label will be added to the MR and the coach will either directly push new commits to the MR, or re-create a new MR with the original changes. 
  - Contributors can mention the coaches in their MRs by typing `@gitlab-org/coaches`.

The list of current merge request coaches can be found in the [team page](/company/team/) by selecting `Merge Request Coach` in the department filter.

There is also the [`#mr-coaching`](https://gitlab.slack.com/archives/C2T9APP9C) channel in GitLab Slack if GitLab team members have any questions related to community contributions.

More information on merge request coaches (including how to become a merge request coach) can be found in the [MR coach lifecycle page](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html).

### Creating educational materials

1. [Gitpod with GDK - Introduction (video)](https://www.youtube.com/watch?v=OzgGP5tT4bo)
1. [Gitpod with GDK - Setup  (video)](https://www.youtube.com/watch?v=6VNm36wdXnI)

### DCO and CLA Guidance

All external contributions to GitLab are subject to the [GitLab DCO or CLA](/community/contribute/dco-cla/), depending on where the contribution is made and on whose behalf.

Instructions for corporate contributors to enter into an overarching Corporate CLA covering all contributions made on their behalf are set out on the [DCO-CLA page](/community/contribute/dco-cla/#need-a-corporate-cla-covering-all-contributors-on-behalf-of-your-organization). 

## Recognizing contributors 

We work with the Community Relations team, to [recognize contributors](/handbook/marketing/community-relations/code-contributor-program/#recognition-for-contributors) 

A nomination process is also available to [nominate a contributor](/handbook/marketing/community-relations/code-contributor-program/community-appreciation/)

## Contributor Efficiency Working group

There's a working group with members from Quality and Community Relations that
aims to streamline and improve contributor efficiency. It implements key
business iterations that results in substantial and sustained increases to
community contributors & contributions.

- Handbook page - <https://about.gitlab.com/company/team/structure/working-groups/contribution-efficiency/>
- Agenda - <https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit#>

## Code Contributor User Journey

The code contributor user journey is documented in the handbook - [User Journey](./user-journey.html)

## Leading Organizations

The GitLab [Leading Organizations program](/handbook/marketing/community-relations/leading-organizations/) is a
cross-functional initiative that is led by the [Contributor Success
team](/handbook/engineering/quality/contributor-success/) and [Community Relations
team](/handbook/marketing/community-relations/). 

The Leading Organization program supports GitLab's [mission](/company/mission/#mission) by recognizing and incentivizing
organizations who are among our most active contributors. A company or individual
[qualifies](/handbook/marketing/community-relations/leading-organizations/#qualification) for unique benefits by
reaching 20 merged merge requests or more over a trailing three full calendar month basis.

