---
layout: handbook-page-toc
title: OKR Management Single-Engineer Group
---

## OKR Management Single-Engineer Group

The OKR Management SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

## Goal
To develop Objectives & Key Results(OKR) functionality within GitLab.


## Philosophy
To create an MVP that is:

* Very loose and un-opinionated to accommodate different styles of OKRs across companies, or departments within the same company
* Separate from planning and project management features, but optional linkage
* Accommodating of non-product development persona use from customer/user organizations

Specific areas that we aim to address within Incubation Engineering are:

* Ability to manually enter KR scoring and calculate rollups. 
* Ability to automatically track the scoring of an objective by linked issues (linear attribution based on open/closed for linked issues).
* Reporting.

## Resources

### Requirement Spec 
Requirements are captured in this EPIC  [https://gitlab.com/groups/gitlab-org/-/epics/7864](https://gitlab.com/groups/gitlab-org/-/epics/7864).

### Issue Tracker
All issues related to work can be found in this EPIC [https://gitlab.com/groups/gitlab-org/incubation-engineering/okr/-/epics/1](https://gitlab.com/groups/gitlab-org/incubation-engineering/okr/-/epics/1)

### Updates
Coming Soon

